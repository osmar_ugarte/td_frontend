import path from 'path';

export default {
  mode: 'development',
  entry: './helpers/polyfills.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build'),
  },
};
