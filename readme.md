# Install

## Babel

> npm install --save-dev @babel/core @babel/cli @babel/register @babel/preset-env browserslist
> npm install regenerator-runtime core-js@3.6.5 --save

## Webpack

> npm install webpack webpack-cli --save-dev

## ESLint

> npm install eslint --save-dev

## Stylelint

> npm install --save-dev stylelint stylelint-config-standard

## Prettier

> npm install --save-dev prettier eslint-config-prettier eslint-plugin-prettier stylelint-config-prettier stylelint-prettier

## Jest

> npm install --save-dev jest babel-jest @babel/core @babel/preset-env

## JSDocs

> npm install --save-dev jsdoc
